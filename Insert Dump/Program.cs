﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insert_Dump
{
    class Program
    {
        static void Main(string[] args)
        {
            clsConnection cls = new clsConnection();

            string[] strArr = {"Date Loaded","Status","Municipality","Property ID","Property #","Product","VPR Service Type","Property  Status","VPR Status",
                                  "Created Date","VPR Status Age","Total Aging","Client Status","Occupancy Status","Assigned Associate","Property Address",
                                  "City","State","Zip code","Vendor","Municipality Name","POD","Follow up Date","Property Type","Borrower Name","parcel number",
                                  "FC/FCH Codes","Foreclosure Docs/ LP Filed (for PFC only)","Registration Conditions","start date","registrant","investor/insurer",
                                  "Investor Name (refer to FITNO tab)","owner/mortgagee/trustee (OMT)","Asset Manager","Asset Manager/Mortgage Servicer (AMS)",
                                  "Property Manager(PM)","Foreclosure Status","Last Inspection Date","Primary Vendor (Vendor Name)","Primary Vendor (Address 1)",
                                  "Primary Vendor (Address 2)","Primary Vendor (City)","Primary Vendor (State)","Primary Vendor (Zip)","Primary Vendor (Contact Name)",
                                  "Primary Vendor (24 hrs emerg# Contact #)","Primary Vendor (Email)","Primary Vendor (Fax)","No# of Units","Street Address","RSFM",
                                  "RSFM Address","Assistant Manager","Assistant Manager2","Website Link","Registration Process","processedBy","processedDate","InitialProcessor",
                                  "HandleTIme","id","PRO ID","lockto","Municipality Code","Tagged By","Date Tagged","Registration Status","Reason","PFC Registration Condition",
                                  "REO Registration Condition","TaggingLockTo","fillTag","Inflow Date","Date Verified","Prochamp Status", "Prio Tag", "Registration Date", 
                                  "Renewal Date", "Complete Form", "Standard Cost"};

            //string asd = "";

            //foreach (string s in strArr)
            //{
            //    asd += s + "\n";
            //}

            string getColumns = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'tbl_VPR_CreatedFile'";

            DataTable dtColumns = cls.GetData(getColumns);

            ArrayList strArr2 = new ArrayList();

            for (int a = 0; a < dtColumns.Rows.Count; a++)
            {
                strArr2.Add(dtColumns.Rows[a][0].ToString());
            }

            DataTable dt = new DataTable();

            string folderpath = @"C:\Users\orculloj\Desktop\Dump\";
            string[] filePaths = Directory.GetFiles(folderpath);

            if (filePaths.Length > 0)
            {
                Console.WriteLine(DateTime.Now + "...File Path: " + filePaths[0]);

                string connectionString = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=YES;IMEX=1;""", filePaths[0]);
                OleDbConnection oleDbConn = new OleDbConnection(connectionString);
                oleDbConn.Open();
                DataTable dt1 = oleDbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string query = "select * from [Sheet1$]";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, connectionString);
                DataSet dataSet = new DataSet();

                Console.WriteLine(DateTime.Now + "...Fill Data Table");

                dataAdapter.Fill(dt);
                oleDbConn.Close();

                //Console.WriteLine(DateTime.Now + "...Fill Data Table.");

                ArrayList arrList1 = new ArrayList();
                ArrayList arrList2 = new ArrayList();

                foreach (DataColumn col in dt.Columns)
                {
                    if (!strArr2.Contains(col.ColumnName))
                    {
                        arrList1.Add(col.ColumnName);
                    }
                    else
                    {
                        arrList2.Add(col.ColumnName);
                    }
                }

                Console.WriteLine(DateTime.Now + "...Removing columns not needed");

                foreach (string c in arrList1)
                {
                    dt.Columns.Remove(c);
                }

                Console.WriteLine(DateTime.Now + "...Adding specific columns");

                foreach (string s in strArr2)
                {
                    if (!arrList2.Contains(s))
                    {
                        dt.Columns.Add(s);
                    }
                }

                Console.WriteLine(DateTime.Now + "...Arrange column in order");

                dt.SetColumnsOrder(strArr2);                

                DataColumn dateLoaded = dt.Columns["Date Loaded"];
                DataColumn status = dt.Columns["Status"];
                DataColumn regDate = dt.Columns["Registration Date"];
                DataColumn renDate = dt.Columns["Renewal Date"];
                DataColumn muniName = dt.Columns["Municipality Name"];
                DataColumn prioTag = dt.Columns["Prio Tag"];

                DataColumn muniCode = dt.Columns["Municipality Code"];
                DataColumn regStatus = dt.Columns["Registration Status"];
                DataColumn product = dt.Columns["Product"];
                DataColumn pfcRegCondition = dt.Columns["PFC Registration Condition"];
                DataColumn reoRegCondition = dt.Columns["REO Registration Condition"];
                DataColumn propType = dt.Columns["Property Type"];
                DataColumn fcCode = dt.Columns["FC/FCH Codes"];
                DataColumn occStatus = dt.Columns["Occupancy Status"];
                DataColumn regProcess = dt.Columns["Registration Process"];
                DataColumn reason = dt.Columns["Reason"];
                DataColumn taggedBy = dt.Columns["Tagged By"];
                DataColumn dateTagged = dt.Columns["Date Tagged"];
                //dateTagged.DataType = System.Type.GetType("System.DateTime");

                Console.WriteLine(DateTime.Now + "...Adding values for \"Date Loaded\" and \"Status\"");

                

                foreach (DataRow row in dt.Rows)
                {
                    row[dateLoaded] = DateTime.Now.ToString("yyyy-MM-dd");
                    if (row[product].ToString() == "PFC")
                    {
                        string qryMuniCode = "select municipality_code from tbl_VPR_Municipality where municipality_name = '" + row[muniName] + "'";
                        DataTable dtMuniCode = cls.GetData(qryMuniCode);

                        string qryPFCSettings = "select * from tbl_VPR_Workable_Registration_PFC where city = '" + dtMuniCode.Rows[0]["municipality_code"].ToString() + "' order by date_modified desc";
                        DataTable dtPFCSettings = cls.GetData(qryPFCSettings);

                        if (dtPFCSettings.Rows.Count > 0)
                        {
                            //if (row[pfcRegCondition].ToString() != "NULL" && !row[pfcRegCondition].Equals(DBNull.Value))
                            //{

                            //}
                            //else
                            //{
                            //    row[regStatus] = "Cancelled";
                            //}

                            if (dtPFCSettings.Rows[0]["pfc_default"].ToString() == "True")
                            {
                                row[regStatus] = "To be registered";
                                row[regProcess] = dtPFCSettings.Rows[0]["type_of_registration"].ToString();
                                row[status] = "";
                                row[taggedBy] = "Auto";
                                row[dateTagged] = DateTime.Now;
                            }
                            else if (dtPFCSettings.Rows[0]["pfc_city_notice"].ToString() == "True")
                            {
                                //row[regStatus] = "To be registered";
                            }
                            else if (dtPFCSettings.Rows[0]["pfc_foreclosure_vacant"].ToString() == "True")
                            {
                                if (row[fcCode].ToString().ToUpper().IndexOf("FC") > -1)
                                {
                                    if (row[occStatus].ToString() == "Vacant" || row[occStatus].ToString() == "Vacant Lot")
                                    {
                                        row[regStatus] = "To be registered";
                                        row[regProcess] = dtPFCSettings.Rows[0]["type_of_registration"].ToString();
                                        row[status] = "";
                                        row[taggedBy] = "Auto";
                                        row[dateTagged] = DateTime.Now;
                                    }
                                    else
                                    {
                                        row[regStatus] = "Not Yet Eligible";
                                        row[reason] = "Occupancy requirement not met";
                                        row[status] = "NW";
                                        row[taggedBy] = "Auto";
                                        row[dateTagged] = DateTime.Now;
                                    }
                                }
                                else
                                {
                                    row[regStatus] = "Suspended";
                                    row[reason] = "No foreclosure filing";
                                    row[status] = "NW";
                                    row[taggedBy] = "Auto";
                                    row[dateTagged] = DateTime.Now;
                                }
                            }
                            else if (dtPFCSettings.Rows[0]["pfc_foreclosure"].ToString() == "True")
                            {
                                if (row[fcCode].ToString().ToUpper().IndexOf("FC") > -1)
                                {
                                    row[regStatus] = "To be registered";
                                    row[regProcess] = dtPFCSettings.Rows[0]["type_of_registration"].ToString();
                                    row[status] = "";
                                    row[taggedBy] = "Auto";
                                    row[dateTagged] = DateTime.Now;
                                }
                                else
                                {
                                    row[regStatus] = "Suspended";
                                    row[reason] = "No foreclosure filing";
                                    row[status] = "NW";
                                    row[taggedBy] = "Auto";
                                    row[dateTagged] = DateTime.Now;
                                }
                            }
                            else if (dtPFCSettings.Rows[0]["pfc_vacant"].ToString() == "True")
                            {
                                if (row[occStatus].ToString().IndexOf("Vacant") > -1 || row[occStatus].ToString() == "PPO")
                                {
                                    if (dtPFCSettings.Rows[0]["pfc_vacant"].ToString() == "True")
                                    {
                                        row[regStatus] = "To be registered";
                                        row[regProcess] = dtPFCSettings.Rows[0]["type_of_registration"].ToString();
                                        row[status] = "";
                                        row[taggedBy] = "Auto";
                                        row[dateTagged] = DateTime.Now;
                                    }
                                    else
                                    {
                                        row[regStatus] = "To be registered";
                                        row[regProcess] = dtPFCSettings.Rows[0]["type_of_registration"].ToString();
                                        row[status] = "";
                                        row[taggedBy] = "Auto";
                                        row[dateTagged] = DateTime.Now;
                                    }
                                }
                                else
                                {
                                    row[regStatus] = "Not Yet Eligible";
                                    row[reason] = "Occupancy requirement not met";
                                    row[status] = "NW";
                                    row[taggedBy] = "Auto";
                                    row[dateTagged] = DateTime.Now;
                                }
                            }
                            else if (dtPFCSettings.Rows[0]["no_pfc_reg"].ToString() == "True")
                            {
                                row[regStatus] = "Cancelled";
                                row[reason] = "REO Registrations Only";
                                row[status] = "NW";
                                row[taggedBy] = "Auto";
                                row[dateTagged] = DateTime.Now;
                            }
                        }
                        else
                        {
                            row[status] = "new";
                        }
                    }
                    else if (row[product].ToString() == "REO")
                    {
                        string qryMuniCode = "select municipality_code from tbl_VPR_Municipality where municipality_name = '" + row[muniName] + "'";
                        DataTable dtMuniCode = cls.GetData(qryMuniCode);

                        string qryREOSettings = "select * from tbl_VPR_Workable_Registration_REO where city = '" + dtMuniCode.Rows[0]["municipality_code"].ToString() + "' order by date_modified desc";
                        DataTable dtREOSettings = cls.GetData(qryREOSettings);

                        if (dtREOSettings.Rows.Count > 0)
                        {
                            //if (row[reoRegCondition].ToString() != "NULL" && !row[reoRegCondition].Equals(DBNull.Value))
                            //{

                            //}
                            //else
                            //{
                            //    row[regStatus] = "Cancelled";
                            //}
                            if (dtREOSettings.Rows[0]["reo_bank_owed"].ToString() == "True")
                            {
                                row[regStatus] = "To be registered";
                                row[regProcess] = dtREOSettings.Rows[0]["type_of_registration"].ToString();
                                row[status] = "";
                                row[taggedBy] = "Auto";
                                row[dateTagged] = DateTime.Now;
                            }
                            else if (dtREOSettings.Rows[0]["reo_vacant"].ToString() == "True")
                            {
                                if (row[occStatus].ToString().IndexOf("Vacant") > -1 || row[occStatus].ToString() == "PPO")
                                {
                                    row[regStatus] = "To be registered";
                                    row[regProcess] = dtREOSettings.Rows[0]["type_of_registration"].ToString();
                                    row[status] = "";
                                    row[taggedBy] = "Auto";
                                    row[dateTagged] = DateTime.Now;
                                }
                                else
                                {
                                    row[regStatus] = "Not Yet Eligible";
                                    row[reason] = "Occupancy requirement not met";
                                    row[status] = "NW";
                                    row[taggedBy] = "Auto";
                                    row[dateTagged] = DateTime.Now;
                                }
                            }
                            else if (dtREOSettings.Rows[0]["reo_city_notice"].ToString() == "True")
                            {
                                row[regStatus] = "To be registered";
                                row[regProcess] = dtREOSettings.Rows[0]["type_of_registration"].ToString();
                                row[status] = "";
                                row[taggedBy] = "Auto";
                                row[dateTagged] = DateTime.Now;
                            }
                            else if (dtREOSettings.Rows[0]["no_reo_reg"].ToString() == "True")
                            {
                                row[regStatus] = "Cancelled";
                                row[reason] = "PFC Registrations Only";
                                row[status] = "NW";
                                row[taggedBy] = "Auto";
                                row[dateTagged] = DateTime.Now;
                            }
                        }
                        else
                        {
                            row[status] = "new";
                        }
                    }

                    //row[dateLoaded] = DateTime.Now.ToString("yyyy-MM-dd");
                    //row[status] = "new";

                    //if (row[muniName].ToString() == "TN - Memphis" ||
                    //    row[muniName].ToString() == "PA - Philadelphia" ||
                    //    row[muniName].ToString() == "NV - North Las Vegas")
                    //{
                    //    row[prioTag] = "1";
                    //}
                    //row[prioTag] = "1";

                    //if (row[regDate].Equals(DBNull.Value) || row[regDate].ToString() == "NULL")
                    //{
                    //    row[regDate] = "";
                    //}

                    //if (row[renDate].Equals(DBNull.Value) || row[renDate].ToString() == "NULL")
                    //{
                    //    row[renDate] = "";
                    //}
                }

                Console.WriteLine(DateTime.Now + "...Row count: " + dt.Rows.Count);

                //cls.SQLBulkCopy("tbl_VPR_CreatedFile", dt);

                string destPath = @"C:\Users\orculloj\Desktop\Dump\Done\" + dt.Rows.Count + " Properties - " + DateTime.Now.ToString("MMddyyyyhhmm") + ".xlsx";

                Console.WriteLine(DateTime.Now + "...Move file to \"Done\" folder");

                //File.Move(filePaths[0], destPath);

                Console.WriteLine(DateTime.Now + "...Done!");
            }
        }


    }

    public static class DataTableExtensions
    {
        public static void SetColumnsOrder(this DataTable table, ArrayList columnNames)
        {
            int columnIndex = 0;
            foreach (string columnName in columnNames)
            {
                table.Columns[columnName].SetOrdinal(columnIndex);
                columnIndex++;
            }
        }
    }
}
